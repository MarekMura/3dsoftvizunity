﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace GraphCore
{
    public class Edge
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public string Label { get; set; }
        public Graph Graph { get; set; }
        public bool IsOriented { get; set; }
        public double Length { get; set; }
        public Node SourceNode { get; set; }
        public Node DestinationNode { get; set; }
        public Color Color { get; set; }

		public Action OnUpdate;

        //Not needed now
        //public double Strength { get; set; }
        //public double Scale { get; set; }
    }
}
