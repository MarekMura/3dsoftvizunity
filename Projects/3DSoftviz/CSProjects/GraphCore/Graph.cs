﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Numerics;
using System.Timers;

namespace GraphCore
{
    public class Graph
    {
        public ObservableCollection<Node> NodeCollection { get; set; }
        public ObservableCollection<Edge> EdgeCollection { get; set; }
        public int NodeSpread = 30;
        public void LoadGraph(int numberOfNodes)
        {
            var rand = new Random();
            NodeCollection = new ObservableCollection<Node>();
            for (var i = 0; i < numberOfNodes; i++)
            {
                var position = new Vector3(rand.Next(1, NodeSpread), rand.Next(1, NodeSpread), rand.Next(1, NodeSpread));
                NodeCollection.Add(new Node() { Position = position });
            }
        }

        public void LoadGraph(int numberOfNodes, int numberOfEdges)
        {
            NodeCollection = new ObservableCollection<Node>();
            EdgeCollection = new ObservableCollection<Edge>();
            var rand = new Random();
            for (var i = 0; i < numberOfNodes; i++)
            {
                var position = new Vector3(rand.Next(1, NodeSpread), rand.Next(1, NodeSpread), rand.Next(1, NodeSpread));
                NodeCollection.Add(new Node() { Position = position });
            }

            for (var i = 0; i < numberOfEdges; i++)
            {
                var source = rand.Next(0, numberOfNodes);
                var destination = rand.Next(0, numberOfNodes);
                if (source == destination ||
                    EdgeCollection.FirstOrDefault(x =>
                        x.SourceNode.Equals(NodeCollection[source]) &&
                        x.DestinationNode.Equals(NodeCollection[destination])) != null
                    || EdgeCollection.FirstOrDefault(x =>
                        x.SourceNode.Equals(NodeCollection[destination]) &&
                        x.DestinationNode.Equals(NodeCollection[source])) != null)
                {
                    i--;
                    continue;
                }

                EdgeCollection.Add(new Edge { SourceNode = NodeCollection[source], DestinationNode = NodeCollection[destination] });
            }

			var timer = new Timer();
			timer.Elapsed += (o, e) =>
			{
				var position = new Vector3(rand.Next(1, NodeSpread), rand.Next(1, NodeSpread), rand.Next(1, NodeSpread));
				var n = new Node() { Position = position };
				NodeCollection.Add(n);

				var edge = new Edge { SourceNode = NodeCollection[new Random().Next(0, NodeCollection.Count)], DestinationNode = n };
				EdgeCollection.Add(edge);

			};
			timer.Interval = 1000;
			timer.Start();

        }
    }
}
