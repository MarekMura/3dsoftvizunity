﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Numerics;
using System.Timers;

namespace GraphCore
{
    public class Node
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public string Label { get; set; }
        public double Scale { get; set; }
        public Color Color { get; set; }
        public  Vector3 Position { get; set; }
        public Node ParentNode { get; set; }
        public Dictionary<string, Edge> Edges { get; set; }

		public Action OnUpdate;

		public Node()
		{
		}

		public void Update()
		{
			OnUpdate();
		}

		//Not needed now
		//public bool HasNestedNode { get; set; }
		//public Graph Graph { get; set; }
		//public double Betweeness { get; set; }
		//public double Closeness { get; set; }
		//Not sure if needed
		//public bool NodeMatched { get; set; }
		//public bool RemovableByUser { get; set; }
		//public bool IsFixed { get; set; }
		//public Vector3 Force { get; set; }
		//public Vector3 Velocity { get; set; }
		//public double OverallWeight { get; set; }
	}
}
