﻿using GraphCore;
using UnityEngine;

public class NodeUnity : MonoBehaviour
{
	private GameObject meshGameObject;
	private Node node;
	private Object mesh;

	public bool IsSelected { get; set; }
	public Node Node
	{
		get { return node; }
		set
		{
			node = value;
			node.OnUpdate += OnUpdate;
		}
	}

	public Object Mesh
	{
		get { return mesh; }
		set
		{
			if (meshGameObject != null)
				Destroy(meshGameObject);

			meshGameObject = (GameObject)Instantiate(value, gameObject.transform);
			meshGameObject.transform.localPosition = new Vector3(0, 0, 0);
			mesh = value;
		}
	}

	private void OnUpdate()
	{
		transform.position = new Vector3(Node.Position.X, Node.Position.Y, Node.Position.Z);
	}
}
