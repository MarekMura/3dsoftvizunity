﻿using GraphCore;
using UnityEngine;

public class EdgeUnity : MonoBehaviour
{
	public bool IsSelected { get; set; }
	public bool IsInvisible { get; set; }
	public Material Material { get; set; }
	public Edge Edge
	{
		get
		{
			return edge;
		}
		set
		{
			edge = value;
			edge.OnUpdate += OnUpdate;
		}
	}

	private Object mesh;

	public Object Mesh
	{
		get { return mesh; }
		set
		{
			if (meshGameObject != null)
				Destroy(meshGameObject);

			meshGameObject = (GameObject)Instantiate(value, gameObject.transform);
			meshGameObject.transform.localPosition = new Vector3(0, 0, 0);
			mesh = value;
		}
	}
	public GameObject meshGameObject;
	private Edge edge;

	private void OnUpdate()
	{

	}

	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{

	}
}
