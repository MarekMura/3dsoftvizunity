﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandsModelChange : MonoBehaviour {

    private GameObject CurrentHandsModel;
	// Use this for initialization
	void Start () {
        // object in LeapHands with "HandsModel" tag
        CurrentHandsModel = GameObject.FindGameObjectWithTag("HandsModel");
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ChangeHandsModel(string name)
    {
        var selectedModel = (GameObject)Instantiate(Resources.Load(string.Format("Prefabs/Leap/LeapHandsCustomized/{0}",name)), gameObject.transform);
        GameObject.Destroy(CurrentHandsModel);
        CurrentHandsModel = selectedModel;
    }
}
