﻿using GraphCore;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using UnityEngine;

public class GraphUnity : MonoBehaviour
{
	private Object nodeMesh;
	private Object edgeMesh;
	public Object NodeMesh
	{
		get
		{
			return nodeMesh ?? Resources.Load("Prefabs/SampleSphereNode");
		}
		set
		{
			nodeScripts.ForEach(x => x.Mesh = value);
			nodeMesh = value;
		}
	}

	public Object EdgeMesh
	{
		get
		{
			return edgeMesh ?? Resources.Load("Prefabs/SampleCylinderEdge");
		}
		set
		{
			edgeScripts.ForEach(x => x.Mesh = value);
			edgeMesh = value;
		}
	}

    //Contains references to unity elements of graph
	private List<NodeUnity> nodeScripts = new List<NodeUnity>();
	private List<EdgeUnity> edgeScripts = new List<EdgeUnity>();

    //Contains reference to graph elements from GraphCore
	private ObservableCollection<Node> nodes;
	private ObservableCollection<Edge> edges;

    //Elements that needs to be add to graph on next update
	private List<Edge> edgesToAdd = new List<Edge>();
	private List<Node> nodesToAdd = new List<Node>();

	// Create graph from nodes 
	public void InitGraph(ObservableCollection<Node> nodes)
	{
		InitCollections(nodes);
		GameObject lastNode = null;
		for (var i = 0; i < nodes.Count; i++)
		{
			var node = CreateNode(nodes[i]);

			if (i != 0)
			{
				CreateEdge(lastNode.transform.position, node.transform.position);
			}
			lastNode = node;
		}
	}
	public void InitGraph(ObservableCollection<Node> nodes, ObservableCollection<Edge> edges)
	{
		InitCollections(nodes,edges);
		foreach (var n in nodes)
		{
			CreateNode(n);
		}

		foreach (var e in edges)
		{
			CreateEdge(e);
		}
	}

	private void InitCollections(ObservableCollection<Node> nodes = null, ObservableCollection<Edge> edges = null)
	{
		this.nodes = nodes;
		this.edges = edges;

		if(this.nodes != null)
			this.nodes.CollectionChanged += OnNodesChanged;

		if(this.edges != null)
			this.edges.CollectionChanged += OnEdgesChanged;
	}

	private GameObject CreateNode(Node n)
	{
		GameObject node = (GameObject)Instantiate(Resources.Load("Prefabs/NodePrefab"), gameObject.transform);
		var nodeScript = node.GetComponent<NodeUnity>();
		nodeScript.Mesh = NodeMesh;
		nodeScript.Node = n;
		nodeScripts.Add(nodeScript);
		node.transform.position = new Vector3(n.Position.X, n.Position.Y, n.Position.Z);

		return node;
	}

	private void CreateEdge(Edge e)
	{
		var source = nodeScripts.FirstOrDefault(x => x.Node.Equals(e.SourceNode));
		var destination = nodeScripts.FirstOrDefault(x => x.Node.Equals(e.DestinationNode));

		CreateEdge(source.transform.position, destination.transform.position);
	}

	private void CreateEdge(Vector3 source, Vector3 destination)
	{
		var distance = Vector3.Distance(source, destination);

		GameObject edge = (GameObject)Instantiate(Resources.Load("Prefabs/EdgePrefab"), gameObject.transform);
		var edgeScript = edge.GetComponent<EdgeUnity>();
		edgeScript.Mesh = EdgeMesh;
		edge.transform.position = source;
		edge.transform.LookAt(destination);
		edge.transform.localScale = new Vector3(edge.transform.localScale.x, edge.transform.localScale.y, distance);
		edgeScripts.Add(edgeScript);
	}

	private void OnNodesChanged(object sender, NotifyCollectionChangedEventArgs ea)
    {
        if (ea.Action != NotifyCollectionChangedAction.Add && ea.NewItems == null)
            return;

		foreach(var n in ea.NewItems)
			nodesToAdd.Add((Node)n);
	}
	private void OnEdgesChanged(object sender, NotifyCollectionChangedEventArgs ea)
	{
		if (ea.Action != NotifyCollectionChangedAction.Add && ea.NewItems == null)
			return;

		foreach (var e in ea.NewItems)
			edgesToAdd.Add((Edge)e);
	}

	private void Update()
	{
        UpdateGraphElements();
	}

    private void UpdateGraphElements()
	{
		if (nodesToAdd.Count > 0)
		{
			foreach (var n in nodesToAdd)
				CreateNode(n);

			nodesToAdd.Clear();
		}
		if (edgesToAdd.Count > 0)
        {
            foreach (var e in edgesToAdd)
                CreateEdge(e);

            edgesToAdd.Clear();
        }
    }
}
