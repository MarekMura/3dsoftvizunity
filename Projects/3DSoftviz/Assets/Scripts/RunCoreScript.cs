﻿using UnityEngine;
using GraphCore;
using System.Collections.Generic;
using System.Collections.ObjectModel;

public class RunCoreScript : MonoBehaviour
{ 
    public GraphUnity GraphUnity { get; set; }
    public Graph Graph { get; set; }
    private bool nodeMeshToggle = true;
    private bool edgeMeshToggle = true;
    private List<string> handsModelsNames;
    private int currentHandModelIndex;

    // Use this for initialization
    void Start ()
    {
        CreateMultiEdgeGraph();
        //CreateSingleEdgeGraph();

        currentHandModelIndex = 0;
        handsModelsNames = new List<string>()
        {
            "HandModelsRigged",
            "HandModelsBalls"
        };
    }
	
	// Update is called once per frame
	void Update ()
    {
        //TEMPORARY UNTILL WE CREATE AN UI
        if (Input.GetKeyDown(KeyCode.K) && nodeMeshToggle)
        {
            GraphUnity.NodeMesh = Resources.Load("Prefabs/SampleCubeNode");
            nodeMeshToggle = !nodeMeshToggle;
        }
        else if (Input.GetKeyDown(KeyCode.K) && !nodeMeshToggle)
        {
            GraphUnity.NodeMesh = Resources.Load("Prefabs/SampleSphereNode");
            nodeMeshToggle = !nodeMeshToggle;
        }
        else if (Input.GetKeyDown(KeyCode.J) && edgeMeshToggle)
        {
            GraphUnity.EdgeMesh = Resources.Load("Prefabs/SampleBoxEdge");
            edgeMeshToggle = !edgeMeshToggle;
        }
        else if (Input.GetKeyDown(KeyCode.J) && !edgeMeshToggle)
        {
            GraphUnity.EdgeMesh = Resources.Load("Prefabs/SampleCylinderEdge");
            edgeMeshToggle = !edgeMeshToggle;
        }

        else if (Input.GetKeyDown(KeyCode.L))
        {
            var LeapHands = GameObject.Find("LeapHands");
            var handsModel = handsModelsNames[currentHandModelIndex];
            LeapHands.GetComponent<HandsModelChange>().ChangeHandsModel(handsModel);
            currentHandModelIndex = (currentHandModelIndex + 1) % handsModelsNames.Count;
        }

	}

    private void CreateSingleEdgeGraph()
    {
        Graph = new Graph();
        Graph.LoadGraph(10);
 
        var graph = (GameObject)Instantiate(Resources.Load("Prefabs/GraphVisualizerPrefab"));
        GraphUnity = graph.GetComponent<GraphUnity>();
        GraphUnity.InitGraph(Graph.NodeCollection);
    }

    private void CreateMultiEdgeGraph()
    {
        Graph = new Graph();
        Graph.LoadGraph(10, 15);

        var graph = (GameObject)Instantiate(Resources.Load("Prefabs/GraphVisualizerPrefab"));
        GraphUnity = graph.GetComponent<GraphUnity>();
        GraphUnity.InitGraph(Graph.NodeCollection, Graph.EdgeCollection);
    }
}
