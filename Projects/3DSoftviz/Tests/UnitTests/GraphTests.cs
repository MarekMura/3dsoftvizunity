﻿
using GraphCore;
using NUnit.Framework;

namespace UnitTests
{
	[TestFixture]
	public class GraphTests
	{
		[Test]
		[TestCase(10)]
		[TestCase(100)]
		[TestCase(1000)]
		public void Graph_LoadGraph(int nodesToCreate)
		{
            var graph = new Graph();

            graph.LoadGraph(nodesToCreate);

			Assert.AreEqual(nodesToCreate, graph.NodeCollection.Count);
		}
	}
}
